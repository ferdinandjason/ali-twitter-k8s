# Ali Twitter

## Description
As a user,
- I want to be able to create a tweet, so that I can express what is my mind.
- I want to be able delete a tweet, so that I can remove a tweet that I don't want to.
- I want to be able to see my tweets, so that I can read tweet I have created before.

This project enable CI and CD pipeline to make deployment faster.

## Table of Contents
- [Ali Twitter](#ali-twitter)
  - [Description](#description)
  - [Table of Contents](#table-of-contents)
  - [Environment Setup](#environment-setup)
    - [VirtualBox](#virtualbox)
    - [Vagrant](#vagrant)
    - [Kubectl](#kubectl)
    - [Minikube](#minikube)
  - [How to Use](#how-to-use)
    - [VM Architecture](#vm-architecture)
    - [Prepare](#prepare)
    - [Ali Twitter Charts](#ali-twitter-charts)
    - [CI/CD Pipeline](#cicd-pipeline)

## Environment Setup
### VirtualBox
You can refer to this [VirtualBox Doc](https://www.virtualbox.org/wiki/Downloads) to download
the virtualbox and install it in your machine.

### Vagrant
You can refer to this [Vagrant Doc](https://www.vagrantup.com/intro/getting-started/install.html) to download
the vagrant and install it in your machine.

### Kubectl
You can refer to this [Kubernetes Doc](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-macos) to download the kubectl and install it in your machine

### Minikube
You can refer to this [Kubernetes Doc](https://kubernetes.io/docs/tasks/tools/install-minikube/) to download the minikube and install it in your machine

## How to Use
### VM Architecture
For more information about VM Architecture please visit [VM Architecture Docs](/docs/vm_infra.md)

### Prepare
1. After clone this projet, run this command to create VM based on above architecture.
   ```bash
    vagrant up
   ```
2. Run this command to start Minikube and create a cluster with driver virtualbox.
   ```
   minikube start --driver=virtualbox
   ```
3. After the VM created, you can run this command to prepare the newly created VM.
   ```bash
    ansible-playbook --inventory=provision/hosts.yml provision/prepare_vm.yml
   ```
4. After that, you need extra step do prepare the GitLab Runner VM. Please visit [Extra Step on GitLab Runner VM Docs](docs/extra_gitlab_runner.md) and follow the instruction.

### Ali Twitter Charts
You can see the [Ali Twitter Charts Docs](alitwitter-charts/README.md) for more information and how to open the application.

You can check the history of deployment via `gitlab-runner` VM and Minikube VM by run this command.
```
helm history alitwitter-release
```

### CI/CD Pipeline
We use GitLab CI/CD Pipeline to test, build, and deploy the app. Please visit [CI/CD Pipeline Docs](docs/ci_cd.md) to see how it's work.
