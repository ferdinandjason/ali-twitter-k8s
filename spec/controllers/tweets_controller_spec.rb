require 'rails_helper'

RSpec.describe TweetsController, type: :controller do
  describe '#index' do
    it 'returns a success response' do
      get :index
      expect(response).to be_successful
    end

    it 'renders the index template' do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe '#new' do
    it 'returns a success response' do
      get :new
      expect(response).to be_successful
    end

    it 'renders the new template' do
      get :new
      expect(response).to render_template('new')
    end
  end

  describe '#create' do
    context 'given valid params' do
      it 'should create a new tweet' do
        post :create, params: { tweet: { body: 'new tweet' } }
        expect(response).to redirect_to(tweets_path)
      end
    end

    context 'given invalid params' do
      it 'returns redirect to new page and shows the error' do
        post :create, params: { tweet: { body: nil } }
        expect(response).to redirect_to(new_tweet_path)
      end
    end
  end

  describe '#delete' do
    it 'should redirect to index page' do
      tweet_to_deleted = Tweet.create body: 'later deleted'
      delete :destroy, params: { id: tweet_to_deleted.to_param }
      expect(response).to redirect_to(tweets_path)
    end

    it 'delete the requested tweet' do
      tweet_to_deleted = Tweet.create body: 'later deleted'
      expect do
        delete :destroy, params: { id: tweet_to_deleted.to_param }
      end.to change(Tweet, :count).by(-1)
    end
  end
end
