require 'rails_helper'

RSpec.describe Tweet, type: :model do
  subject { Tweet.new }

  context 'given a valid body to tweet' do
    it 'should be valid' do
      subject.body = 'tweet body'
      expect(subject).to be_valid
    end
  end

  context 'given an empty body to tweet' do
    it 'should be invalid' do
      subject.body = nil
      expect(subject).to be_invalid
    end
  end

  context 'given more than 140 characters to tweet' do
    it 'should be invalid' do
      subject.body = '1234567890123456789012345678901234567890' \
                    '1234567890123456789012345678901234567890' \
                    '1234567890123456789012345678901234567890' \
                    '123456789012345678901'
      expect(subject).to be_invalid
    end
  end
end
