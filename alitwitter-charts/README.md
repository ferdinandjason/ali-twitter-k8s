# Ali Twitter Chart
A simple twitter application made for Ali

As a user,
- I want to be able to create a tweet, so that I can express what is my mind.
- I want to be able delete a tweet, so that I can remove a tweet that I don't want to.
- I want to be able to see my tweets, so that I can read tweet I have created before.

## Prerequisites

- Kubernetes
- Helm 3

## TL;DR;
```bash
helm install alitwitter-release .
helm uninstall gate-release
```

## Introduction
This chart bootstraps a twitter application deployment on [Kubernetes](http://kubernetes.io) cluster using [Helm](https://helm.sh) package manager.

## Installing the Chart
To install this chart, run this command
```bash
helm install alitwitter-release .
```

Run this command from this project directory to deploys Gate on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameter that can be configured during installation.

## Uninstalling the Chart
To uninstall/delete the `alitwitter-release`:

```bash
helm uninstall gate-release
```

## Parameters
The following tables lists the configurable parameters of the Ali Twitter chart and their default values.

Parameter                           | Description                                               | Default
----------------------------------- | --------------------------------------------------------- | -------------
`replicaCount`                      | Node replicas (deployment)                                | `1`
`image.repository`                  | `alitwitter` image repository                             | `ferdinandjason/alitwitter`
`image.tag`                         | `alitwitter` image tag                                    | `1.0-alpine`
`image.pullPolicy`                  | Image pull policy                                         | `IfNotPresent`
`service.type`                      | Service type                                              | `NodePort`
`service.port`                      | Service port                                              | `80`
`config.database.adapter`           | Database adapter for Ali Twitter App (PostgreSQL/SQLite3) | `postgresql`
`config.database.username`          | Database username                                         | `admin`
`config.database.password`          | Database password                                         | `admin`
`config.database.ip`                | Database adapter host                                     | `postgresql-release`
`config.database.port`              | Database adapter port                                     | `5432`
`config.database.development`       | Database name for development environment                 | `twitter-development`
`config.database.test`              | Database name for test environment                        | `twitter-test`
`config.database.production`        | Database name for production environment                  | `twitter`
`config.env`                        | The value for `RAILS_ENV`                                 | `production`
`config.secretKeyBase`              | The value for `SECRET_KEY_BASE`                           | `secret`

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example,

```console
$ helm install alitwitter-release \
  --set config.database.username=username,config.database.password=password \
    .
```

The above command sets the Ali Twitter database `username` account password to `password`.

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

```console
$ helm install alitwitter-release -f my-values.yaml .
```

## How to Run
1. Get the application URL by running these commands:
```bash
export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services alitwitter-release)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
echo http://$NODE_IP:$NODE_PORT
```
2. Go to Browser and go to `http://$NODE_IP:$NODE_PORT/tweets`
