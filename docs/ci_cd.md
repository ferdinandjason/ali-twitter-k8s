# CI/CD Pipeline

## Table of Contents
- [CI/CD Pipeline](#cicd-pipeline)
  - [Table of Contents](#table-of-contents)
  - [Diagram](#diagram)
  - [Pipeline](#pipeline)
    - [Test](#test)
    - [Build](#build)
    - [Deploy](#deploy)

## Diagram
![IMG](img/CI_CD&#32;Devops&#32;Week&#32;Two.png)

## Pipeline
### Test
1. Set the Environtment Variables
2. Installing Gem by `bundle install`
3. Create the test database `bundle exec rake db:create`
4. Testing the app by `bundle exec rspec spec`

### Build
1. Build docker image by `docker build -t ferdinandjason/alitwitter:1.0-alpine .`
2. Push the docker image to docker hub `sudo docker push ferdinandjason/alitwitter:1.0-alpine`

### Deploy
1. Deploy the up by using helm upgrade by `helm upgrade -i alitwitter-release alitwitter-charts`
