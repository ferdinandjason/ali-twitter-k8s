# Virtual Machine Architecture on Virtualization
These docs will explain about Virtual Machine Architecture on Virtualization.

## Table of Contents
- [Virtual Machine Architecture on Virtualization](#virtual-machine-architecture-on-virtualization)
  - [Table of Contents](#table-of-contents)
  - [Architecture Image](#architecture-image)
  - [Architecture](#architecture)


## Architecture Image
![IMG](img/VM&#32;Infrastructure&#32;DevOps&#32;Week&#32;Two.png)

## Architecture
If you do `vagrant up` command it will make several VM with belom specification.

| Name | IP Address | OS | RAM | Description | Username |
| - | - | - | - | - | - |
| `gitlab-runner` | `192.168.100.100` | `Ubuntu 18.04` | `1024 MB` | VM for GitLab Runner Service | `vagrant` |

You can check the VM status by run this command
```bash
vagrant status
```
