# Extra Step on GitLab Runner VM

## Table of Contents
- [Extra Step on GitLab Runner VM](#extra-step-on-gitlab-runner-vm)
  - [Table of Contents](#table-of-contents)
  - [Login Docker Hub](#login-docker-hub)
  - [Change Password for gitlab-runner user](#change-password-for-gitlab-runner-user)
  - [Add gitlab-runner user to sudoers](#add-gitlab-runner-user-to-sudoers)
  - [Registering a GitLab Runner VM](#registering-a-gitlab-runner-vm)

## Login Docker Hub
1. Login by run this command
   ```bash
   docker login --username=[USERNAME] --password=[PASSWORD]
   ```

## Change Password for gitlab-runner user
1. Go to `su` mode, by run this command.
   ```bash
   sudo su
   ```
2. Change the `gitlab-runner` user
   ```bash
   passwd gitlab-runner
   ```
3. And follow the instruction given by system.


## Add gitlab-runner user to sudoers
1. You can grant sudo permissions to the gitlab-runner user as this is who is executing the build script.
   ```bash
   sudo usermod -a -G sudo gitlab-runner
   ```
   You now have to remove the password restriction for sudo for the gitlab-runner user.

2. Start the sudo editor with
   ```bash
   sudo visudo
   ```

3. Now add the following to the bottom of the file
   ```
   gitlab-runner ALL=(ALL) NOPASSWD: ALL
   ```

## Registering a GitLab Runner VM
1. Go to GitLab Runner VM, by run this command
   ```bash
   vagrant ssh gitlab-runner
   ```
2. Follow the instruction in [Official Registering Runners Docs](https://docs.gitlab.com/runner/register/#gnulinux) and use `shell` runner.

I use shell runner so that `test` and `build` can be done faster because of cache. So the CI/CD process can fast enough and fulfil the DevOps purpose.
